import styled from "styled-components";

export const ArtStyle = styled.div`
  padding: 100px 50px;
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  overflow-y: auto;
  background-image: url("/images/background7.png");
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  background-attachment: fixed;
`;
